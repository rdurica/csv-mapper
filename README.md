CSV Mapper
=====
Csv mapper is a simple library to  parse csv files and convert to Json/Object

Highlights
-------

* Transform CSV documents into object (stdClass)
* Convert columns into bool, int, float, string
* First row is used as accessor 
* Empty cells or '#N/A' return null instead of empty string
* Sum operation on columns available
* Allow modify time limit,memory limit, csv delimiter

System Requirements
-------

You need **PHP >= 7.2.0** to use Csv mapper but the latest stable version of PHP is recommended.


Install
-------

Install using Composer.

```
$ composer require robbyte/csv-mapper
```
Base usage
-------
Create an instance & set file
```php
$mapper = new \Robbyte\CsvMapper\Mapper();
$mapper->setFile('your_file.csv');
```
Request result
```php
$yourVariable = $mapper->getObject()
```

Setters
-------

```php
$mapper->setDelimiter(','); // string - set delimiter to csv. Default = ";"
$mapper->setDataType(['int', 'bool', 'float', 'int', 'int']);  // array - Convert columns to specified types
$mapper->setMaxExecutionTime(-1); // int - set max_execution_time. Default = 60
$mapper->setMemoryLimit(-1); // int - set memory_limit. Default = 2048
```

Getters
-------
```php
$mapper->getSum('id'); // Return sum of specified column
$mapper->getCount('email'); // Return count of specified column
```

License
-------
The MIT License (MIT). Please see [LICENSE](LICENSE) for more information.
