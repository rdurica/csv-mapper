# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.0"></a>
# 0.1.0 (2018-07-11)


### Features

* **package.json:** Added package.json file for changelog ([05f68fb](https://gitlab.com/rdurica/csv-mapper/commit/05f68fb))
* **strict types:** enabled strict types ([849773c](https://gitlab.com/rdurica/csv-mapper/commit/849773c))
