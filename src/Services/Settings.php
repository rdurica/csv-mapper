<?php
declare(strict_types=1);

namespace Robbyte\CsvMapper\Services;

/**
 * Class Settings
 * @package Robbyte\CsvMapper\Services
 */
class Settings
{

    /** @var int */
    private $memoryLimit;

    /** @var string */
    private $delimiter;

    /** @var int */
    private $maxExecutionTime;

    /** @var array */
    private $types;

    /** @var string */
    private $file;

    /**
     * Settings constructor.
     */
    public function __construct()
    {
        $this->maxExecutionTime = 60;
        $this->memoryLimit = 2048;
        $this->delimiter = ';';
        $this->types = [];
    }

    /**
     * @return int
     */
    public function getMemoryLimit(): int
    {
        return $this->memoryLimit;
    }

    /**
     * @param int $memoryLimit
     */
    public function setMemoryLimit(int $memoryLimit): void
    {
        $this->memoryLimit = $memoryLimit;
    }

    /**
     * @return string
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter(string $delimiter): void
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @return int
     */
    public function getMaxExecutionTime(): int
    {
        return $this->maxExecutionTime;
    }

    /**
     * @param int $maxExecutionTime
     */
    public function setMaxExecutionTime(int $maxExecutionTime): void
    {
        $this->maxExecutionTime = $maxExecutionTime;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array $types
     */
    public function setTypes(array $types): void
    {
        $this->types = $types;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile(string $file): void
    {
        $this->file = $file;
    }


}