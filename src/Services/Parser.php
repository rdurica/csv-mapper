<?php
declare(strict_types=1);

namespace Robbyte\CsvMapper\Services;

/**
 * Class Parser
 * @package Robbyte\CsvMapper\Services
 */
class Parser
{
    /** @var array */
    private $headers;

    /** @var array */
    private $result;

    /**
     * @param  Settings $settings
     * @return array
     */
    public function createArray(Settings $settings): array
    {
        ini_set('memory_limit', (string)$settings->getMemoryLimit());
        ini_set('max_execution_time', (string)$settings->getMaxExecutionTime());

        $file = fopen($settings->getFile(), 'r');
        $i = 0;
        while (($line = fgetcsv($file, 0, $settings->getDelimiter())) !== false) {

            if ($i === 0) {
                $this->createHeader($line);
            } else {
                $this->result['row' . $i] = $this->createObject($line, $settings->getTypes());
            }

            $i++;
        }

        fclose($file);

        return $this->result;
    }


    /**
     * @param array $items
     * @param array $types
     * @return \stdClass
     */
    private function createObject(array $items, array $types): object
    {
        $result = [];

        foreach ($items as $key => $item) {
            if (!$item || $item === '#N/A') {
                $result[$this->headers[$key]] = null;
                continue;
            }

            if (empty($types)) {
                $result[$this->headers[$key]] = $item;
                continue;
            }

            $newItem = $this->retypeItem($types, $key, $item);

            $result[$this->headers[$key]] = $newItem;
        }

        return (object)$result;
    }

    /**
     * @param array $items
     * @return void
     */
    private function createHeader(array $items): void
    {
        foreach ($items as $key => $item) {
            $this->headers[$key] = (string)preg_replace("/\-{2,}/u", "-", mb_strtolower($item));
        }
    }

    /**
     * @param array $types
     * @param int $key
     * @param string $item
     * @return bool|float|int|string
     */
    private function retypeItem(array $types, int $key, string $item)
    {
        $types[$key] = $types[$key] ?? 'string';

        switch ($types[$key]) {
            case 'bool':
                $newItem = (boolean)$item;
                break;
            case 'string':
                $newItem = (string)$item;
                break;
            case 'int':
                $newItem = (int)$item;
                break;
            case 'float':
                $newItem = (float)$item;
                break;
            default:
                $newItem = $item;
                break;
        }

        return $newItem;
    }
}