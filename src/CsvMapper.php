<?php
declare(strict_types=1);

namespace Robbyte\CsvMapper;

require_once 'Services/Parser.php';
require_once 'Services/Settings.php';

use Robbyte\CsvMapper\Services\Parser;
use Robbyte\CsvMapper\Services\Settings;


/**
 * Class CsvMapper
 * @package Robbyte\CsvMapper
 */
class CsvMapper
{
    /** @var Parser */
    protected $parser;

    /** @var Settings */
    private $settings;

    /** @var array */
    private $result;

    /**
     * CsvMapper constructor.
     */
    public function __construct()
    {
        $this->parser = new Parser();
        $this->settings = new Settings();
    }

    /**
     * @param string $delimiter
     * @return CsvMapper
     */
    public function setDelimiter(string $delimiter): CsvMapper
    {
        $this->settings->setDelimiter($delimiter);
        return $this;
    }

    /**
     * @param int $memoryLimit
     */
    public function setMemoryLimit(int $memoryLimit): void
    {
        $this->settings->setMemoryLimit($memoryLimit);
    }

    /**
     * @param int $maxExecutionTime
     */
    public function setMaxExecutionTime(int $maxExecutionTime): void
    {
        $this->settings->setMaxExecutionTime($maxExecutionTime);
    }

    /**
     * @param array $types
     * @return CsvMapper
     */
    public function setDataType(array $types): CsvMapper
    {
        $this->settings->setTypes($types);
        return $this;
    }

    /**
     * @param  string $file
     * @return CsvMapper
     */
    public function setFile(string $file): CsvMapper
    {
        $this->settings->setFile($file);
        return $this;
    }


    /**
     * @return \stdClass
     */
    public function getObject(): object
    {
        $this->result = $this->parser->createArray($this->settings);

        return (object)$this->result;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return count($this->result);
    }


}